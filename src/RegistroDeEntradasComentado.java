
/**
 *
 * Dado um arquivo com registro de entradas de pessoas em um banco, deve ser
 * mostrada a quantidade de pessoas que entraram no banco no período de
 * expediente [das 10:00 às 16:00].
 *
 * Cada registro no arquivo de log possui o seguinte formato: [AAAA-MM-DD
 * hh:mm:ss]
 *
 * Sendo: AAAA - ano com 4 dígitos MM - mês com dois dígitos DD - dia com dois
 * dígitos hh - horas (de 00 a 23) com dois dígitos mm - minutos com dois
 * dígitos ss - segundos com dois dígitos
 *
 * Dias e horários inválidos (como 29/02/2007 e 16:68:12) devem ser ignorados na
 * contagem.
 *
 * OBS.: o programa deve ler a entrada a partir de um arquivo em texto plano
 * contendo registros separados por quebras de linha.
 *
 */
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;
import javax.swing.JOptionPane;

public class RegistroDeEntradasComentado {

    public static void main(String[] args) throws FileNotFoundException {
        String[] entradas = null;
//        String caminho = JOptionPane.showInputDialog("Digite o caminho do arquivo:");
        String caminho = "/home/m4/NetBeansProjects/RegistroDeEntradas/src/arquivo";
        entradas = lerArquivo(caminho);
        int quandidadeDePessoas = 0;
        for (int i = 0; i < entradas.length; i++) {
            if (validarRegistro(entradas[i])) {
                quandidadeDePessoas++;
            }
        }
        System.out.println(quandidadeDePessoas + " pessoas entraram no banco no periodo das 10:00 as 16:00 \n");
    }

    // recebe como parâmetro um array com as linhas do arquivo
    public static boolean validarRegistro(String registro) {
        boolean valido = true;
        String hora, minuto, segundo, data;
//        pega a hora minuto e segundos de acordo com o tamanho
        hora = registro.substring(12, 14);
        minuto = registro.substring(15, 17);
        segundo = registro.substring(18, 20);
        data = registro.substring(1, 11);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        
        if (data.trim().length() != dateFormat.toPattern().length()) {
            valido =  false;
        }
        dateFormat.setLenient(false);
        
        try {
            //parse the inDate parameter
            dateFormat.parse(data.trim());
        } catch (ParseException pe) {
            valido = false;
        }
        

        System.out.println(hora + ":" + minuto + ":" + segundo);

        if (Integer.parseInt(hora) < 0
                || Integer.parseInt(hora) > 23
                || Integer.parseInt(minuto) < 0
                || Integer.parseInt(minuto) > 59
                || Integer.parseInt(segundo) < 0
                || Integer.parseInt(segundo) > 59) {
            valido = false;
        }
        return valido;
    }

    /**
     * Recebe o caminho como parâmetro Retorna um vetor de strings
     *
     * @param caminho
     * @return
     * @throws FileNotFoundException
     */
    public static String[] lerArquivo(String caminho) throws FileNotFoundException {
        int quantidadeDeRegistros = 0;

        Scanner scanner;
//        useDelimiter é um certo tipo de expressão regular.
//        No caso abaixo ignora quebras de linha
        scanner = new Scanner(new FileReader(caminho)).useDelimiter("\\n");
//        hasNext() retorna true se o scanner tem outro token a seguir, caso
//        contrário retorna false
//          ==================================
//        O OBJETIVO DESTA PARTE É PEGAR A QUANTIDADE DE REGISTROS
        while (scanner.hasNext()) {
//            scanner.next() vai ler a proxima linha
            scanner.next();
            quantidadeDeRegistros++;
        }

//        cria um array de strings do tamanho da quantidade de registros
        String[] registros = new String[quantidadeDeRegistros];
        int contador = 0;
//        useDelimiter é um certo tipo de expressão regular.
//        No caso abaixo ignora quebras de linha
//          ==================================
//        O OBJETIVO DESTA PARTE É SALVAR NAQUELE ARRAY DO TAMANHO DA QUANTIDADE
//       OS DADOS
        scanner = new Scanner(new FileReader(caminho)).useDelimiter("\\n");
        while (scanner.hasNext()) {
            registros[contador] = scanner.next();
            contador++;
        }
//        retorna um array com os registros
        return registros;
    }

}
