import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author 
 */
public class RegistroDeEntradasComentadoTest {
    
    public RegistroDeEntradasComentadoTest() {
    }

//    /**
//     * Test of main method, of class RegistroDeEntradasComentado.
//     */
//    @Test
//    public void testMain() throws Exception {
//        System.out.println("main");
//        String[] args = null;
//        RegistroDeEntradasComentado.main(args);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
    /**
     * Test of validarRegistro method, of class RegistroDeEntradasComentado.
     */
    @Test
    public void testValidarRegistroValido() {
        String registro = "[2010-03-05 12:00:02]";
        boolean result = RegistroDeEntradasComentado.validarRegistro(registro);
        assertEquals(true, result);
    }
    
    @Test
    public void testValidarRegistroLimiteHorario1() {
        String registro = "[2007-02-28 23:59:59]";
        boolean result = RegistroDeEntradasComentado.validarRegistro(registro);
        assertEquals(true, result);
    }
    
    @Test
    public void testValidarRegistroLimiteHorario2() {
        String registro = "[2007-02-28 00:00:00]";
        boolean result = RegistroDeEntradasComentado.validarRegistro(registro);
        assertEquals(true, result);
    }
    
    @Test
    public void testValidarRegistroHoraInvalida1() {
        String registro = "[2007-02-28 24:00:02]";
        boolean result = RegistroDeEntradasComentado.validarRegistro(registro);
        assertEquals(false, result);
    }
    
    @Test
    public void testValidarRegistroHoraInvalida2() {
        String registro = "[2007-02-28 -1:00:02]";
        boolean result = RegistroDeEntradasComentado.validarRegistro(registro);
        assertEquals(false, result);
    }
    
    @Test
    public void testValidarRegistroMinutoInvalido1() {
        String registro = "[2007-02-28 23:60:02]";
        boolean result = RegistroDeEntradasComentado.validarRegistro(registro);
        assertEquals(false, result);
    }
    
    @Test
    public void testValidarRegistroMinutoInvalido2() {
        String registro = "[2007-02-28 23:-1:02]";
        boolean result = RegistroDeEntradasComentado.validarRegistro(registro);
        assertEquals(false, result);
    }
    
    @Test
    public void testValidarRegistroSegundoInvalido1() {
        String registro = "[2007-02-28 23:59:60]";
        boolean result = RegistroDeEntradasComentado.validarRegistro(registro);
        assertEquals(false, result);
    }
    
    @Test
    public void testValidarRegistroSegundoInvalido2() {
        String registro = "[2007-02-28 23:59:-1]";
        boolean result = RegistroDeEntradasComentado.validarRegistro(registro);
        assertEquals(false, result);
    }
    
//    ==========
    
    @Test
    public void testValidarRegistroLimiteHora1() {
        String registro = "[2007-02-28 00:00:02]";
        boolean result = RegistroDeEntradasComentado.validarRegistro(registro);
        assertEquals(true, result);
    }
    
    @Test
    public void testValidarRegistroLimiteHora2() {
        String registro = "[2007-02-28 23:00:02]";
        boolean result = RegistroDeEntradasComentado.validarRegistro(registro);
        assertEquals(true, result);
    }
    
    @Test
    public void testValidarRegistroLimiteMinuto1() {
        String registro = "[2007-02-28 23:00:02]";
        boolean result = RegistroDeEntradasComentado.validarRegistro(registro);
        assertEquals(true, result);
    }
    
    @Test
    public void testValidarRegistroLimiteMinuto2() {
        String registro = "[2007-02-28 23:59:02]";
        boolean result = RegistroDeEntradasComentado.validarRegistro(registro);
        assertEquals(true, result);
    }
    
    @Test
    public void testValidarRegistroLimiteSegundo1() {
        String registro = "[2007-02-28 23:00:00]";
        boolean result = RegistroDeEntradasComentado.validarRegistro(registro);
        assertEquals(true, result);
    }
    
    @Test
    public void testValidarRegistroLimiteSegundo2() {
        String registro = "[2007-02-28 23:00:59]";
        boolean result = RegistroDeEntradasComentado.validarRegistro(registro);
        assertEquals(true, result);
    }
    
    
//    ==========
    
    
    @Test
    public void testValidarRegistroDiaInvalido() {
        String registro = "[2007-02-29 23:00:59]";
        boolean result = RegistroDeEntradasComentado.validarRegistro(registro);
        assertEquals(false, result);
    }
    
    @Test
    public void testValidarRegistroMesInvalido() {
        String registro = "[2007-13-28 23:00:59]";
        boolean result = RegistroDeEntradasComentado.validarRegistro(registro);
        assertEquals(false, result);
    }  
    
    

    /**        
     * Test of lerArquivo method, of class RegistroDeEntradasComentado.
     */
    @Test
    public void testLerArquivo1Entrada() throws Exception {
        String caminho = "/home/m4/NetBeansProjects/RegistroDeEntradas/src/arquivosLeitura/arquivo0";
        String[] expResult = new String[1];
        expResult[0] = "[2010-03-05 12:00:02]";
        String[] result = RegistroDeEntradasComentado.lerArquivo(caminho);
        assertArrayEquals(expResult, result);
    }
    
    @Test
    public void testLerArquivo4Entradas() throws Exception {
        String caminho = "/home/m4/NetBeansProjects/RegistroDeEntradas/src/arquivosLeitura/arquivo1";
        String[] expResult = new String[4];
        expResult[0] = "[2012-12-24 24:58:00]";
        expResult[1] = "[2007-03-05 12:00:00]";
        expResult[2] = "[2016-02-29 16:68:12]";
        expResult[3] = "[2015-02-29 09:68:12]";
        String[] result = RegistroDeEntradasComentado.lerArquivo(caminho);
        assertArrayEquals(expResult, result);
    }
    
    @Test
    public void testLerArquivoCaracteresInvalidos() throws Exception {
        String caminho = "/home/m4/NetBeansProjects/RegistroDeEntradas/src/arquivosLeitura/arquivo2";
        String[] expResult = new String[1];
        expResult[0] = "asdf@3";
        String[] result = RegistroDeEntradasComentado.lerArquivo(caminho);
        assertArrayEquals(expResult, result);
    }
    
    @Test
    public void testLerArquivoCaractereInvalidoNoMeio() throws Exception {
        String caminho = "/home/m4/NetBeansProjects/RegistroDeEntradas/src/arquivosLeitura/arquivo3";
        String[] expResult = new String[3];
        expResult[0] = "[2012-12-24 24:58:00]";
        expResult[1] = "asdf@3";
        expResult[2] = "[2015-02-29 09:68:12]";
        String[] result = RegistroDeEntradasComentado.lerArquivo(caminho);
        assertArrayEquals(expResult, result);
    }
    
}
